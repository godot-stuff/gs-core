class_name Configuration
extends Node


func load_config(config_file : String):
	
	Logger.trace("[Configuration] _load")
	var _config = ConfigFile.new()
	var _config_filename = config_file

	if not FileAccess.file_exists(_config_filename):
		Logger.warn("Configuration File {name} is missing.".format({"name": _config_filename}))
		return null

	Logger.debug(_config_filename)
	var _err = _config.load(_config_filename)
	
	if (_err > 0):
		Logger.error("Error {0} Reading Configuration File.".format([_err]))
		return null
		
	return _config

