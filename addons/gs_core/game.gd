class_name Game
extends Node

var data : RefCounted
var config_filename : String = ""

signal on_config_loading()
signal on_config_loaded(configuration: RefCounted)
signal on_game_starting()
signal on_game_loaded()
signal on_game_quiting()
signal on_game_quit()

var __quit_cancel: bool = false
var __config_data = {}
var __config_filename: String = "res://game.cfg"
const VERSION="1.0-R1"
##
## Quit Game
##
## Helper to Quit a Game
func quit():
	
	Logger.trace("[Game] quit")
	
	Logger.trace("- quiting the game")
	on_game_quiting.emit()
	
	if __quit_cancel:
		Logger.trace("- quit game was cancelled")
		return
		
	get_tree().quit()
	

func load_config(config_filename):
	
	Logger.trace('[Game] load_config')
	
	Logger.trace('- loading configuration file %s' % config_filename)
	on_config_loading.emit()
	
	var _config = Configuration.new()
	var _config_file = _config.load_config(config_filename) as ConfigFile

	on_config_loaded.emit(__config_data)
	
#func get_config_filename():
#	return "res://game.cfg"
#
#
#func get_data_reference():
#	return GameData.new()
#
#
#func on_load_data(config_data: ConfigFile):
#	pass
	
	
func _init():
	
	print(" ")
	print("godot-stuff Game")
	print("https://gitlab.com/godot-stuff/gs-game")
	print("Copyright 2018-2023, SpockerDotNet LLC")
	print("Version " + VERSION)
	print(" ")
	
	randomize()


func _ready():
	
	Logger.trace("[Game] _ready")
	
	load_config(__config_filename)
	
#	var _config = Configuration.new()
#	var _config_data = _config.load_config(get_config_filename()) as ConfigFile
#
#	if (!_config_data):
#		Logger.warn("Your Game Config File is Missing.")
#		return
#
#	data = _load_data(_config_data)
#	on_load_data(_config_data)
		
#	print()
#	print(data.name)
#	print(data.long_version)
#	print(data.copyright)
#	print()


func _load_data(config_data : ConfigFile):
	
	var _data = get_data_reference()
	
	_data.name = config_data.get_value("game", "name")
	_data.version = config_data.get_value("game", "version")
	_data.long_version = "Version " + _data.version
	_data.copyright = config_data.get_value("game", "copyright")
	_data.message = config_data.get_value("game", "message")

	return _data
