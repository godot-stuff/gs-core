class_name GameResource
extends Resource

var name : String = "My Game"
var version : String = "1.0.0"
var long_version : String = "Version 1.0"
var copyright : String = "Copyright 9999 Some Company, All Rights Reserved."
var message : String = "Hello World"
