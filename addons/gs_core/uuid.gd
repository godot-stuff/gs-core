
#
#	Copyright 2018, SpockerDotNet LLC
#
#	GDScript implementation on RFC4122 for creating
#	a UUID from a Random or Pseudo-Random Number
#
#	http://www.ietf.org/rfc/rfc4122.txt

extends Reference
class_name UUID

var uuid setget , _get_uuid


func create():
	#return _create_uuid()
	return v4()


func _get_uuid():
	return uuid


func _create_uuid():

	var s = []
	var hex = "0123456789abcdef"

	for i in range(37):
		s.append(hex.substr(floor(rand_range(0,16)), 1))

	s[14] = "4"
	s[19] = hex.substr((s[19] && 3) || 8, 1)
	s[8] = "-"
	s[13] = "-"
	s[18] = "-"
	s[23] = "-"

	var uuid = ""
	for i in range(s.size()-1):
		uuid += s[i]

	return uuid


static func getRandomInt(max_value):
	randomize()
	return randi() % max_value

static func randomBytes(n):
	var r = []

	for index in range(0, n):
		r.append(getRandomInt(256))

	return r


static func uuidbin():

	var b = randomBytes(16)

	b[6] = (b[6] & 0x0f) | 0x40
	b[8] = (b[8] & 0x3f) | 0x80
	return b


static func v4():

	var b = uuidbin()

	var low = '%02x%02x%02x%02x' % [b[0], b[1], b[2], b[3]]
	var mid = '%02x%02x' % [b[4], b[5]]
	var hi = '%02x%02x' % [b[6], b[7]]
	var clock = '%02x%02x' % [b[8], b[9]]
	var node = '%02x%02x%02x%02x%02x%02x' % [b[10], b[11], b[12], b[13], b[14], b[15]]

	return '%s-%s-%s-%s-%s' % [low, mid, hi, clock, node]


func _init():
	
	randomize()
	uuid = create()

