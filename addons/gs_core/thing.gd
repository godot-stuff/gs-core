#	A Thing is Something that exists in a Game
#
#	@remarks
#	A Thing is Nothing but it is Everything. For that
#	reason, Everything in the Game must be a Thing.
#
#	@copyright
#	Copyright 2018, SpockerDotNet LLC

extends Reference
class_name Thing

#	@type:uuid The Unique Identifier
var id = "" setget , _get_id
#	@type:bool Is this Thing Active
var is_active = true
#	@type:bool Is this Thing Visible
var is_visible = true



func _get_id():
	
	#	Getter for the Id for this Thing
	#
	#	@returns
	#	@uuid A Unique Identifier (UUID)
	
	return id.uuid


func _init():
	id = UUID.new()
	
