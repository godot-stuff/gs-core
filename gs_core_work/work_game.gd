class_name __WorkGame__
extends Game


func get_config_filename():
	return "res://gs_core_work/game.cfg"
	
	
func get_data_reference():
	return WorkGameData.new()
	
	
func on_load_data(config_data: ConfigFile):
	data.test_string = config_data.get_value("test", "string")
	data.test_int = config_data.get_value("test", "int")
	data.test_float = config_data.get_value("test", "float")
	
	
func _ready():
	print(data.test_string)
	print(data.test_int)
	print(data.test_float)
	
